import { HttpService } from '@nestjs/axios';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

@Injectable()
export class ZohoService {
  header = { Authorization: '' };

  constructor(private httpService: HttpService) {}

  async refreshToken() {
    const resp = await this.httpService
      .post(
        'https://accounts.zoho.com/oauth/v2/token?refresh_token=' +
          process.env.REFRESH_TOKEN +
          '&client_id=' +
          process.env.CLIENT_ID +
          '&client_secret=' +
          process.env.CLIENT_SECRET +
          '&grant_type=' +
          process.env.GRANT_TYPE,
      )
      .toPromise();
    this.httpService.axiosRef.defaults.headers.common[
      'Authorization'
    ] = `Zoho-oauthtoken ${resp.data.access_token}`;
  }
  async sendPost(url: string, data: any) {
    await this.refreshToken();
    const resp = await this.httpService.post(url, data).toPromise();
    console.log('cree ID*******');
    console.log(resp.data.data[0]);
    console.log(resp);
    return resp.data;
  }

  async crearLeadNota(data: any) {
    const nota = data.Nota;
    delete data.Nota; //eliminamos la variable para no incurrir en error
    const idLead = await this.sendPost(
      `${process.env.ZOHO_API_CRM_RESOURCE}/Leads`,
      data,
    );
    const dataNote = {
      data: [
        {
          Parent_Id: `${idLead}`,
          se_module: 'Leads',
          Note_Title: 'Nota del lead',
          Note_Content: `${nota}`,
        },
      ],
    };
    const idNote = await this.sendPost(
      `${process.env.ZOHO_API_CRM_RESOURCE}/Notes`,
      dataNote,
    );
  }
}
