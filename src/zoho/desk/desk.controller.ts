import { Body, Controller, Get, Post } from '@nestjs/common';
import { ZohoService } from '../zoho.service';

@Controller('desk')
export class DeskController {
    constructor(private zohoService:ZohoService){}
    
    @Get("")
    index(){
        return "estas en desk";
    }

    @Post("/tickets")
    crearTicker(@Body() body){
        return this.zohoService.sendPost(`${process.env.ZOHO_API_DESK_RESOURCE}/tickets`,body);
    }
}

