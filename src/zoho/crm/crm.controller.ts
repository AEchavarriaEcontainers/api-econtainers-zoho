import { Body, Controller, Get, Post } from '@nestjs/common';
import { ZohoService } from '../zoho.service';

@Controller('crm')
export class CrmController {
  constructor(private zohoService: ZohoService) {}

  @Get('')
  index() {
    return 'crm con aut';
  }

  @Post('/Leads')
  crearLead(@Body() data: any) {
    console.log('Recibir Leads *******');
    console.log(data);
    return this.zohoService.sendPost(
      `${process.env.ZOHO_API_CRM_RESOURCE}/Leads`,
      data,
    );
  }
  @Post('/Leads/Notes')
  crearLeadNotes(@Body() data: any) {
    console.log('Recibir Leads  con nota*******');
    console.log(data);
    this.zohoService.crearLeadNota(data);
  }
}
