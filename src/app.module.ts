import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ZohoService } from './zoho/zoho.service';
import { DeskController } from './zoho/desk/desk.controller';
import { CrmController } from './zoho/crm/crm.controller';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [HttpModule,ConfigModule.forRoot()],
  controllers: [AppController, CrmController, DeskController],
  providers: [AppService, ZohoService],
})
export class AppModule {}
